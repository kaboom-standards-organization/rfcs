# RFC #0001 - Code Guides

## Preface

This RFC details the targeted scope and formatting of [Guide wiki](https://code.chipmunk.land/kaboom-standards-organization/guide/wiki) pages which guide the reader on utilizing specific protocol/bot libraries.

## Scope

The guides for these libraries must not introduce the reader to the basic concept of programming and the syntax of the programming languages the bot libraries target.

However, as these guides are geared towards beginners to bot development, it is still very likely that there will still be some people entirely new to programming.
As such, deviations from conventional wisdom should be well documented - for example, this would mean documenting the fact that in most programming languages, indexes start from 0 instead of 1.
Such things must be documented, even if they aren't the aim of the code guides, as they will inevitably need to be tackled in bot development.

It may be useful for advanced bot developers to occasionally check the guides as well to see if their bots are still using the accepted best practices.

Guides must detail the best available practices for what they are describing at the time they are written.

It must also attempt to illustrate unique design choices in the bot libraries it describes.

## Structure

### Tooling

The guide should attempt to tell the user what programs (IDEs, compilers and IDE extensions) they are recommended to use in relation to the language and bot library, and should also mention some alternatives and the reason why they're not recommended.

#### Examples

A guide about a library for Javascript would recommend that the user should install Visual Studio Code and use the npm package manager but also mention that yarn exists.
A guide about a library for Rust would recommend that the user should install Visual Studio Code, install Rust with rustup, use the cargo package manager, use the rust-analyzer extension but also mention that JetBrains provide RustRover.
A guide about a library for Java would recommend that the user should install IntelliJ Idea, install the JDK through IntelliJ and use Gradle for compilation but also mention that Eclipse and Maven exist.

### Useful Resources

Guides should also link to resources that may be useful when troubleshooting issues not covered by the guide and for general library discovery.

For example, all guides would link to [Wiki VG](https://wiki.vg) (or it would be linked in the common ground page) and a guide for node-minecraft-protocol would link to [minecraft-data documentation](https://prismarinejs.github.io/minecraft-data/?v=1.20.4&d=protocol).

### Troubleshooting

The author of the guide should also attempt to identify common pitfalls in bot development (for example, not responding to the clientbound position update packet with an accept teleport packet, that sort of thing) and add a section to the guide, under a Troubleshooting section, that details why the issue occurs and how to solve it.

## Version Target

Guides must target the latest version of the library released that supports the current server version (not the latest version that ViaVersion allows). While node-minecraft-protocol is an outlier in that the latest version supports every protocol version, most libraries only support one protocol version in one major version.

If there is a newer version available for the library than the documentation is written for, and the server version is not the same as the latest protocol version the newer library version targets, guide authors should attempt to write migration instructions for the new protocol version.

If there is a newer version available for the library than the documentation is written for, and the server version is the same as the latest protocol version the newer library version targets, the guide title must be updated to include a suffix of `(OUTDATED)`. Once the documentation has been updated for the new version, the suffix must be removed.

If there is not a new version available for the library than the documentation is written for, and the server version is above the protocol version the latest version of the library targets, a suffix must not be appended if the library is maintained. If the library is no longer maintained, an `(UNMAINTAINED)` tag should be added to the documentation title. No more updates to documentation for unmaintained libraries is expected from contributors, however you are welcome to add it.

### Guide Targets

Guides must attempt to illustrate, at the bare minimum:

1. How to get a bot to connect to the server, and send a chat message of "Hello world!", without the quotes, on login
2. How to get a bot to parse system chat and output, without color codes, to stdout
3. How to get a bot to listen for player chat messages, and check their content for a specific keyword and then reply to it.

Guides should also attempt to tackle:

1. Registry-based profileless/player chat packet parsing
2. ANSI log output and color code parsing
3. Implementing a command queue
4. Implementing a chat queue
5. Tracking rectifiable property states (i.e. OP and gamemode)
6. Correcting rectifiable property states with queue-skipping command execution

Guides should offer the reader tips on how to:

1. Parse chunks
2. Maintain command cores
3. Rectify properties silently
4. Not lag the server

## Common Ground

Guides are likely to have some sort of common ground.
As such, to avoid repeating ourselves in documentation and to make updating documentation easier when something major in the protocol changes, a page that details common ground between protocol libraries should be established.
This common ground page would be appended to once we identify duplication between code guides, and would be linked to whenever the common ground would come up in such guides.